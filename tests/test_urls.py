"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django tests *

:details: lara_django application urls tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from lara_django.models import

# Create your lara_django tests here.
