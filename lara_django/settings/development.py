"""
________________________________________________________________________

:PROJECT: lara-django

*lara-django development settings*

:details: Django settings for lara project.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180623

.. note:: -
.. todo:: -
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.2.7"

import os
import sys
import logging

from lara_django.settings.settings_base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# include lara core apps
LARA_APPS = env.list("LARA_APPS", default=['lara_django_base',
                                           'lara_django_people', 'lara_django_library', 'lara_django_store',
                                           'lara_django_material', 'lara_django_material_store',
                                           'lara_django_organisms',  'lara_django_organisms_store',
                                           'lara_django_samples',
                                           'lara_django_sequences', 'lara_django_substances', 'lara_django_substances_store',
                                           'lara_django_data', 'lara_django_processes', 'lara_django_projects'])

DJNAGO_DEV_APPS = [
    # this is only important in the development env.
    'django.contrib.staticfiles',
    'django_extensions',
]

# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps


INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + DJNAGO_DEV_APPS + LARA_APPS

LOGIN_REDIRECT_URL = 'index'
LOGOUT_REDIRECT_URL = 'index'

# Absolute path to the media directory
# this is not required in devel
#STATIC_ROOT = env('STATIC_ROOT', default=os.path.join('static/'))

# should be adjusted
# MEDIA_ROOT = os.path.join(STATIC_ROOT, "media")
# MEDIA_ROOT = os##.path.join(PROJECT_PATH, 'media') # Absolute path to the media directory
# ~ MEDIA_ROOT =  media_path # os.path.join(PROJECT_PATH, media_path )
# ~ os.environ["DJANGO_MEDIA_ROOT"] = MEDIA_ROOT

#DJANGO_MEDIA_PATH = BASE_DIR

# should be removed or set by env
# os.path.join(DJANGO_MEDIA_PATH, "evaluation"),
DESTINATION_BASE = env('DESTINATION_BASE', default=os.path.join('/', 'tmp'))

# ~ print(" -~~~~ {}".format(MEDIA_ROOT) )

# might not be required in development !!
STATICFILES_DIRS = [
    #BASE_DIR / "static",
    #DJANGO_MEDIA_PATH  # media_path
]

STATIC_ROOT = env("STATIC_ROOT", default='/srv/web/django-static')

MEDIA_ROOT = env("MEDIA_ROOT", default=BASE_DIR)