"""
________________________________________________________________________

:PROJECT: lara-django

*lara-django development settings*

:details: Django settings for lara project.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180623
:date: (last modification) 20180627

.. note:: -
.. todo:: -
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.6"

import os
import sys
import logging
import environ

from lara_django.settings.settings_base import *

env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)

# include lara core apps
LARA_APPS = env.list("LARA_APPS", default=['lara_django_base',
                                           'lara_django_people', 'lara_django_library',
                                           'lara_django_material', 'lara_django_parts', 'lara_django_containers', 'lara_django_devices',
                                           'lara_django_sequences',
                                           'lara_django_substances', 'lara_django_substances_store',
                                           'lara_django_data', 'lara_django_processes', 'lara_django_projects'])


INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LARA_APPS

LOGIN_REDIRECT_URL = 'index'
LOGOUT_REDIRECT_URL = 'index'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/


# should be checked:
STATIC_URL = '/static/'

STATIC_ROOT = env('STATIC_ROOT', default=os.path.join(
    '/', 'data', 'web', 'django-static'))
#print("stat root: ", STATIC_ROOT)
MEDIA_ROOT = os.path.join(STATIC_ROOT, "media")
# MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media') # Absolute path to the media directory
# ~ MEDIA_ROOT =  media_path # os.path.join(PROJECT_PATH, media_path )
# ~ os.environ["DJANGO_MEDIA_ROOT"] = MEDIA_ROOT
# DJANGO_MEDIA_PATH = "/tmp" # env('DJANGO_MEDIA_PATH', ".")
# media_path = '/mnt/lc1dat/data/lara_django'  #os.environ["DJANGO_MEDIA_PATH"]  # Absolute path to the media directory

# should be removed
DESTINATION_BASE = os.path.join(MEDIA_ROOT, "evaluation"),

#~ print(" -~~~~ {}".format(MEDIA_ROOT) )

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static/"),
    MEDIA_ROOT,  # DJANGO_MEDIA_PATH  # media_path
]

# CACHES
# ------------------------------------------------------------------------------
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env("REDIS_URL", default=''),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            # Mimicing memcache behavior.
            # https://github.com/jazzband/django-redis#memcached-exceptions-behavior
            "IGNORE_EXCEPTIONS": True,
        },
    }
}

# # SECURITY
# # ------------------------------------------------------------------------------
# # https://docs.djangoproject.com/en/dev/ref/settings/#secure-proxy-ssl-header
# SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
# # https://docs.djangoproject.com/en/dev/ref/settings/#secure-ssl-redirect
# SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=True)
# # https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-secure
# SESSION_COOKIE_SECURE = True
# # https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-secure
# CSRF_COOKIE_SECURE = True
# # https://docs.djangoproject.com/en/dev/topics/security/#ssl-https
# # https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-seconds
# # TODO: set this to 60 seconds first and then to 518400 once you prove the former works
# SECURE_HSTS_SECONDS = 60
# # https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-include-subdomains
# SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
#     "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True
# )
# # https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-preload
# SECURE_HSTS_PRELOAD = env.bool("DJANGO_SECURE_HSTS_PRELOAD", default=True)
# # https://docs.djangoproject.com/en/dev/ref/middleware/#x-content-type-options-nosniff
# SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
#     "DJANGO_SECURE_CONTENT_TYPE_NOSNIFF", default=True
# )
