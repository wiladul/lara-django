
import logging
from django.utils.module_loading import import_string
from django_socio_grpc.settings import grpc_settings

#from lara_base.views import HomeMenu, SignUp

# ~ from sila_device_manager.views import hello, current_datetime

logger = logging.getLogger(__name__)

def grpc_handlers(server):
    """
    Add servicers to the server
    """

    logger.info("adding grpc handlers now")

    if len(grpc_settings.user_settings['GRPC_HANDLERS']) == 0:
        logger.warning(
            "No servicers configured. Did you add GRPC_HANDLERS list to GRPC_FRAMEWORK settings?")

    for handler_str in grpc_settings.user_settings['GRPC_HANDLERS']:
        logger.debug(f"Adding servicers from {handler_str}")
        print(f"****Adding servicers from {handler_str}")
        register_handler = import_string(handler_str)
        register_handler(server)  # calling each handler registration
