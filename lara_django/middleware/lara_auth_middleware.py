# Authentification / Authorisation for all pages - s. https://stackoverflow.com/questions/66857893/apply-django-authentication-for-all-views

from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


class AuthPageProtectionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        # TODO: wrong or circular redirect ...
        # if request.user.is_authenticated:
        #     if not request.user.is_admin:
        #         return self.redirect_to_login()
        # else:
        #     return self.redirect_to_login()

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def redirect_to_login(self):
        return HttpResponseRedirect(reverse_lazy('login'))
