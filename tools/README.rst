lara release tools
===================


installation on pipy
-----------------------

For details, see `PyPi packaging <https://packaging.python.org/tutorials/packaging-projects/>`_.

.. code-block:: console

  # installing required packages
  python3 -m pip install --upgrade setuptools wheel twine

  # create package
  python3 setup.py sdist bdist_wheel

  # upload it to test.pipy
  python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

  # check everything (in new venv)
  python2 -m venv test-venv
  source test-venv/bin/activate
  # do not install dependencies, use --no-deps option since pypi does not have the same packages
  python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps example-pkg-your-username
  python
  >>>import [my_package]

  # if no errors occurred, install on the real pypi with twine

  twine upload dist/*
