#!/usr/bin/env python3
"""_____________________________________________________________________

:PROJECT: lara-django

*lara-django installer skript*

:details: lara-django installer.
          Installing core LARA python-django environment.
          
          - core settings and database
          - 
          
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180627
________________________________________________________________________
"""

import sys
import os
from configparser import ConfigParser, NoSectionError, NoOptionError
from django.core.management import  call_command

from lara_django.lara.management.simpleinstall import query_yes_no, query, call, runSetup

USER_HOME_DIR = os.environ.get('HOME')
REPO_DIR = os.path.dirname(os.path.realpath(__file__)) # current directory of this repository

# try reading configuration from system config files
LARA_CONFIG_FILE = default_answer=os.path.join(USER_HOME_DIR, '.config', 'lara-suite', 'lara-django', 'lara-django.conf') 
lara_config_file = False

lara_config = ConfigParser()
try:     
    lara_config.read(LARA_CONFIG_FILE)
    #~ lara_config_file = True
    
except Exception as err :
    sys.stderr.write("ERROR: Please add section in your config file {}".format(err) )
    lara_config_file = False


installer_welcome_txt = ("_____________________________________________\n\n"
                         "   __      __    ____    __                  \n"
                         "  (  )    /__\  (  _ \  /__\                 \n"
                         "   )(__  /(__)\  )   / /(__)\                \n"
                         "  (____)(__)(__)(_)\_)(__)(__)               \n\n"
                         "        I N S T A L L E R                    \n"
                         " I will guide you through the complete       \n"
                         " installation of the LARA core ...           \n"
                         " [type: ?    - for further information       \n"
                         "        help - to list all input options]    \n"
                         "_____________________________________________\n\n")

print(installer_welcome_txt)

# Install virtual Python3 environment ?
inst_venv = query_yes_no("Install a virtual Python environment (recommended) ?", 
                          help="HELP: This is the recommended installation mode for testing LARA-django")
if inst_venv:
    venv_dir = query("Please specify a DIRECTORY for your virtual python3 environment", 
                     default_answer=os.path.join(USER_HOME_DIR,"python3","lara_venv"), 
                     help="HELP: specify the target directory for the virtual python3 environment")
    
    create_venv_anyway = True
   
    if os.path.exists(venv_dir): 
        create_venv_anyway = query_yes_no("\nWARNING !! Virtual environment exists: [{}], shall I create it anyway ?".
                                          format(venv_dir), default_answer='no', 
                                          help="HELP: Really create the python 3 virtual environment anyway ?")
    
    if create_venv_anyway:
        try: # should be replaced by importAndInstall, when this is working
            import venv
        except ImportError:
            import pip
            pip.main(['install', 'venv'])
            import venv
        #installAndImport('venv') # making sure, that venv package is available
        
        print("\t...creating venv in dir [{}]".format(venv_dir))
        if venv is not None: 
            venv.create(venv_dir, system_site_packages=False, clear=False, symlinks=False, with_pip=True)
            
            # mkdir 
            venv_dirs = [ os.path.join(venv_dir,'database'),
                          os.path.join(venv_dir, 'media'),
                          os.path.join(venv_dir, 'static'),
                          os.path.join(venv_dir, 'to_monitor')]
            map(os.makedirs, venv_dirs ) 
        
    print("* Activating venv [{}]".format(venv_dir)) # this is done by prepending python3 path to sytem path
    print("* ATTENTION:  Before using, always activate your virtual environment with:\n\n\t  source {}/bin/activate\n".format(venv_dir))
    os.environ['PATH'] = os.pathsep.join( [ os.path.join(venv_dir, 'bin'), os.environ['PATH'] ] )

# Ask intallation type ( test or production) ?
intallation_type = query(("Installation type? [Please select number]\n"
                          "1\t- testing/development installation (default, django testing webserver) \n"
                          "2\t- production-installtion (Apache2/ngix)\n", 
                     default_answer="1", 
                     help="HELP: specify the installation type. A test installtion uses the python django. Type a number.")
    
if intallation_type == "1":
    print("Installing test/development evironment (python django webserver) - [type:{}]".format(intallation_type))
    #~ from django.core.management import  call_command
    
    # Install lara dependancies ?  ( pip3 is faster than dependancies from setup.py)
    if query_yes_no("Install lara-django dependancies (recommended) ?", 
                     help="HELP: This will install all required packages"):
        print("Installing devel dependancies ...")
        requirements = os.path.join(REPO_DIR, 'requirements', 'devel.txt')
        
        if inst_venv:
            call('pip install --upgrade pip')
            call('pip install -r ' + requirements )
        else:
            import pip
            pip.main(['install', '-r' , requirements])
            
    # check, if config file exists, if yes, use it        
    if lara_config_file:
        try:
            print(lara_config['media']['DJANGO_MEDIA_PATH'])
            
        except NoSectionError as err :
            sys.stderr.write("ERROR: Please add section in your config file {}".format(err) )
            lara_config_file = False
        except KeyError as err:
            sys.stderr.write("ERROR: Cannot read lara-django config file option in {}".format(err) )
            lara_config_file = False
            
    # else generate new config file
    else : # install config file ;  user might be asked here ...
        lara_config['database'] = {'DB_PATH':  os.path.join(venv_dir, 'database') } # path to media
        lara_config['media'] = {'DJANGO_MEDIA_PATH':  os.path.join(venv_dir, 'media') } # path to media
        lara_config['static'] = {'DJANGO_STATIC_PATH' : os.path.join(venv_dir, 'static') } # path to static files
        lara_config['fixture'] = {'DJANGO_FIXTURE_PATH': os.path.join(venv_dir, 'static') } # path to static fixtures
        lara_config['evaluation'] = {'LARA_TO_MONITOR_PATH': os.path.join(venv_dir, 'to_monitor'), #  # path to directory that is monitored
                                     'BASE_DATA_DESTINATION' : os.path.join(venv_dir, 'media') } #  /path/to/evaluation/targets
        lara_config['email'] = {'EMAIL_HOST': 'localhost',  # 
                                'SERVER_EMAIL' : 'django@localhost', 
                                'error_mail'   : 'django@localhost' , # full name: email_address@domain.xx
                                '404_mail'     : 'django@localhost'}  # full name: email_address@domain.xx
        
        with open(LARA_CONFIG_FILE, 'w') as configfile:
            sys.stderr.write("WARNING: Writing configuration to config file {}, please check !".format(v) )
            lara_config.write(configfile)
            
    # init db
    # call_command('init_db', '-r') 

    exit()
    
elif intallation_type == "2":
    print("Installing production environment with Apache2/Ngix - not implemented yet [type:{}]".format(intallation_type))
    
    if query_yes_no("Install lara-django dependancies (recommended) ?", 
                     help="HELP: This will install all required packages"):
    
        print("Installing production dependancies ...")
        requirements = os.path.join(REPO_DIR, 'requirements', 'production.txt')
        
        if inst_venv:
            call('pip3 install -r ' + requirements )
        else:
            import pip
            pip.main(['install', '-r' , requirements])
    
    exit()
    

# Install libraries ?
if query_yes_no("Install SiLA2 libraries (recommended) ?", 
                 help="HELP: This will install core SiLA2 libraries"):
    print("Installing libraries ...")
   
    if inst_venv:        
        runSetup(src_dir=os.path.join(REPO_DIR, 'sila_library'), 
                 lib_dir=venv_dir)
        
    # Install SiLA java bridge ?
    if query_yes_no("Also install the SiLA2 JAVA bridge (optional, if JAVA binding will be used) ?", 
                     default_answer="no", 
                     help="HELP: This will install an alternative SiLA-JAVA bridge. This is only required, if you intend to use the JAVA bridge"):
        print("Installing SiLA2 JAVA bridge ... - not implemented yet")
else:
    sys.stdout.write("Well, what a pitty ... - you should really consider installing the SiLA libraries \n")

# Install SiLA tools ?
if query_yes_no("Install SiLA2 tools, like the codegenerator (recommended) ?", help="HELP: This will install core SiLA2 tools, like codegenerator"):
    print("Installing tools ...")
    
    requirements = os.path.join(REPO_DIR, 'sila_tools', 'requirements_base.txt')
    
    if inst_venv:
        call('pip3 install -r ' + requirements )
    else:
        import pip
        pip.main(['install', '-r' , requirements])

    print(" !!! please install codegenerator manually by calling python3 setup.py install in sila_tools directory \n ")

    runSetup(src_dir=os.path.join(REPO_DIR, 'sila_tools'), 
                 lib_dir=venv_dir)
                 
    #~ tools_setup = os.path.join(REPO_DIR, 'sila_tools','setup.py')
    #~ print(tools_setup)
    #~ run_setup(tools_setup,  script_args=['install'])
    #~ cmd.call(os.path.join(venv_dir, 'bin', 'python3') + " " + tools_setup + " install" )
    
# Generate documentation ?
if query_yes_no("Generate documentation (recommended) ?", 
                 help="HELP: This will generate the documentation"):
    print("Generating documentation ...")

if inst_venv:
    print("Attention: Please do not forget to activate the virtual environment by calling: \n"
           "source {}/bin/activate \n"
           "- to deactivate the venv, simply type:\n"
           "deactivate \n"
           "Enjoy the SiLA2 with Python3 :)\n").format(venv_dir)
