#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""
________________________________________________________________________

:PROJECT: 

*brief summary*

:details: :

          - python manage.py dumpdata lara.Currency --indent 2 --natural-primary > /tmp/1_curr_fix.json

:file:    csv_JSON_generator.py

:author:  mark doerr <mark@MicT660p> : contrib.

:version: 0.0.1

:date: (creation)          20190704
:date: (last modification) 20190704
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

import os
import sys
import csv
import json
import logging

def csv2JSON(csv_filename = '',
             lara_model = "lara.???", 
             fix_filename_full = '' ):
    """ :param csv_filename: name of input csv file
        :param lara_model: this refers to the database table
        :param fix_filename_full: fixture output file in JSON format
    """
    
    # this refers to the database table
    
    with open(csv_filename) as csvfile:
        csv_reader = csv.DictReader(csvfile)
        sys.stdout.write(self.style.SUCCESS("now adding substances from [{}]...".format(csv_filename) ) )
        
        i = 1
        fix_item_lst = []
        curr_idx_dic = {}
        for row in csv_reader:
            logging.debug(substance_row)
            
            item_dic = dict()
            
            current_fix_item = dict( model=lara_model, pk=i, fields=item_dic )
            
            fix_item_lst.append(current_fix_item)
            i += 1
    
    #~ print(json.dumps(fix_item_lst, indent=2) )
    
    # write fixture file
    with open( fix_filename_full, 'w') as f: json.dump(fix_item_lst, f, indent=2)
   

if __name__ == '__main__':
    """Main: """
    
    fixture_filename = '1__fix.json'
    
    root_path = "/" # OS dependent
    fixture_target_path = os.path.join( root_path, 'tmp','fixtures')
    
    try:  
        os.makedirs(fixture_target_path)
    except OSError:  
        print("Creation of the directory %s failed - does it already exist ?" % fixture_target_path)
    else:  
        print("Successfully created the directory %s" % fixture_target_path )
            
    fix_filename_full = os.path.join( fixture_target_path, fixture_filename )
    
    csv2JSON(csv_filenname = ".csv" , fix_filename_full=fix_filename_full)
        
        
        
    
    
