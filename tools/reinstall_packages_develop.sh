#! /bin/sh

# development installation of lara

lara_root_dir=$(pwd)
echo LARA root directory: $lara_root_dir


cd lara-django/lara_django && pip install -e . && cd $lara_root_dir \
&& cd lara-django-base && pip install -e . && cd $lara_root_dir \
&& cd lara-django-data && pip install -e . && cd $lara_root_dir \
&& cd lara-django-people && pip install -e .  && cd $lara_root_dir \
&& cd lara-django-store && pip install -e . && cd $lara_root_dir \
&& cd lara-django-library && pip install -e . && cd $lara_root_dir \
&& cd lara-django-material && pip install -e . && cd $lara_root_dir \
&& cd lara-django-material-store && pip install -e . && cd $lara_root_dir \
&& cd lara-django-substances && pip install -e . && cd $lara_root_dir \
&& cd lara-django-substances-store && pip install -e . && cd $lara_root_dir \
&& cd lara-django-organisms && pip install -e . && cd $lara_root_dir \
&& cd lara-django-organisms-store && pip install -e . && cd $lara_root_dir \
&& cd lara-django-samples && pip install -e . && cd $lara_root_dir \
&& cd lara-django-processes && pip install -e . && cd $lara_root_dir \
&& cd lara-django-projects && pip install -e . && cd $lara_root_dir 

#~ cd lara_python_qt5 && pip install -e . && cd $lara_root_dir \
#~ cd lara-simpleprocman && pip install -e . && cd $lara_root_dir \
