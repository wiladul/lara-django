# LARA-django

The core LARA project database and webservice, hosting LARA-django applications for planning, storing and presenting experimental data and its meta data, realised as a python-django project


### Installation

Run development version with docker-compose:

```bash
 wget https://gitlab.com/LARAsuite/lara-django/-/blob/master/docker/docker-compose.dev.yml
 wget https://gitlab.com/LARAsuite/lara-django/-/blob/master/docker/.env.dev
 # or with curl:
 curl -O https://gitlab.com/LARAsuite/lara-django/-/blob/master/docker/docker-compose.dev.yml
 curl -O https://gitlab.com/LARAsuite/lara-django/-/blob/master/docker/.env.dev

 # important: rename .env.dev -> .env
 mv .env.dev .env

 # important: replace 'latest' in DOCKER_IMAGE_TAG=latest by current short git commit hash (first 8 characters of hash) 
 # example: DOCKER_IMAGE_TAG=4298aded

 # to pull the docker containers from the github container registry, run in the directory where the docker-compose file is located:
 docker-compose -f docker-compose.dev.yml  pull

 # to start the docker containers, run in the directory where the docker-compose file is located (add -d to run in the background):
 docker-compose -f docker-compose.dev.yml up
```

## Creating new entries

http://127.0.0.1:8000/admin/ 

to create some entries (you'll need the Admin app enabled).


## Environment variables

for development, please set

    export DJANGO_ALLOWED_HOSTS=localhost
    export DJANGO_SETTINGS_MODULE=lara_django.settings.devel

for production, please set 

    export DJANGO_SETTINGS_MODULE=lara_django.settings.production

if your media does not reside in the default media folder, please set
environment variable to 

    export DJANGO_MEDIA_PATH='path/to/my/media'

to use user defined fixtures, please set: :: export
    
    DJANGO_FIXTURE_PATH='path/to/user/fixtures'


Testing all applications

## Basic Commands

### Type checks

Running type checks with mypy:

    $ mypy lara_django

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report:

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

#### Running tests with pytest

    $ pytest


[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: GPLv3

